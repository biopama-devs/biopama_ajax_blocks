/**
 * @file
 */
var bool_biopama_ajax_blocks = false;
(function ($) {
	Drupal.loadBlockOnAjax = function () {
	    $('div[id^=load-block-on-ajax-wrapper--]').click(function () {
			if( false == bool_biopama_ajax_blocks ) {
			  bool_biopama_ajax_blocks = true;
		      var arr_block_id = $(this).attr('id').split("--");
		      var str_block = $(this);
		      $.ajax({
		        url: drupalSettings.path.baseUrl + "ajax-block/" + arr_block_id[1],
		        method :'GET',
		        dataType: "json",
		        success: function (data) {
		        	str_block.append( '<img src="' + drupalSettings.path.baseUrl + 'modules/custom/biopama_ajax_blocks/images/loader.gif" alt="Loading..." height="42" width="42">' )
		        	str_block.html( data );
		        }
		      });
			}
	    });
	}
	$(document).ready(function () {
		Drupal.loadBlockOnAjax();
	});

})(jQuery);
