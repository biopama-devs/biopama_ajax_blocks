<?php

namespace Drupal\biopama_ajax_blocks\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Defines LoadBlockController class.
 */
class biopama_ajax_blocksController {

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
    public function loadBlock( $block_machine_name ) {

      $block = \Drupal\block\Entity\Block::load($block_machine_name);
//       print_r($block->);
      $block_content = '';
      if( $block ) {
        $block_content = \Drupal::entityTypeManager()->getViewBuilder('block')->view($block);
        ($block_content);
      }
      
      $block_print = \Drupal::service('renderer')->renderRoot($block_content);

//         $block_config = \Drupal\block\Entity\Block::load($block_machine_name);
//         $uuid = $block_config->getPlugin()->getDerivativeId(); 
//         $block_content = \Drupal::service('entity.repository')->loadEntityByUuid('block_content', $uuid);

      return new JsonResponse(Drupal\Core\Render\RendererInterface::render($block_print));

  }

}
